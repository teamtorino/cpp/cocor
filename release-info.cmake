
set( RELEASE_MAJOR            2   )
set( RELEASE_MINOR            0   )
set( RELEASE_REVISION         1   )

set( RELEASE_NAME       "alpha"   )

# variables coming from .gitlab-ci.yml (empty outside build-pipeline):

set( RELEASE_EPOCH     $ENV{SOURCE_DATE_EPOCH}   )
set( RELEASE_DATE      $ENV{SOURCE_DATE_DATE}    )
set( RELEASE_TIME      $ENV{SOURCE_DATE_TIME}    )


